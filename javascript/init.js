var inin = true;//pour ne pas relancer les écoutes dans le reste du jeu
var disSortie = 100;
// animation debut jeu
//alert("Tu peux jouer avec ta souris, comme avec les fleches duclavier et la touche entrée.");
function initialisation(){
    $("#start").click(function(){
        if(inin){
            $("#gamename").animate({
            	margin: '0',
            });
            // faire disparaitre le bouton start
            $('#start').css("display", "none");
            // pour que iut laby bien possitionné
            $("#gamename").css('text-align','left');
            // faire apparetre score et stress
            $("li").css('display', 'block');
            // commencer le jeu 
            $("#jeu").css('display', 'block');
            $("#jeu").animate({
                height: '80vh',
            	width: '90%',
            	left: '0',
            	top: '0'
            });
            introduction();
        }
    });
};

initialisation();